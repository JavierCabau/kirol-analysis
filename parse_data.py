# Script for parse the mined data from kirol info

# import libraries
import pandas as pd
import os


def parse_data(path, final_name):
    """
    :param path:
    :param final_name
    :return: date format: day-month-hour-minute
    """
    def date(day_, month_,  hour_, minute_):
        """
        :param month_:
        :param day_:
        :param hour_:
        :param minute_:
        :return:
        """
        date_formated = str(day_) + '-' + str(month_) + '-' + str(hour_) + '-' + str(minute_)
        return date_formated

    def format_data(da):
        """
        da formato a los datos
        :param da:
        :return:
        """
        # set column name
        da_column = da[0].split()
        month = da_column[-5]
        hour = da_column[-1].split('_')[0]
        minute_raw = da_column[-1].split('_')[1]
        if int(minute_raw) <= 10:
            minute = '0'
        else:
            minute = '30'
        day = da_column[-3]
        date_formated = date(day, month, hour, minute)
        column_d = [date_formated]

        # set names and ocupation
        ocupation = []
        names = []
        for i_ in range(len(da)):
            da_list = da[i_].split()
            ocup = da_list[-7]
            name = ' '.join(da_list[:-7])
            ocupation.append(ocup)
            names.append(name)

        result = pd.DataFrame(ocupation, columns=column_d, index=names)
        return result

    list_files = os.listdir(path)
    first = True
    for i in list_files:
        with open(path + i) as file:
            contents = file.readlines()
        if first:
            data_final = format_data(contents)
            first = False
        else:
            data_now = format_data(contents)
            data_final = pd.concat([data_final, data_now], axis=1)
    data_final.to_csv(path + final_name + '.csv')
    return
