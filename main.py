# import libraries
from selenium import webdriver
from datetime import datetime
import time
from parse_data import parse_data
# todo: introducir un formato fecha.


def search_selenium_kirol(pisci=False):
    """
    Function to take info from kiroltegi website
    :param pisci: True if pisci
    :return:
    """
    if pisci:
        link_to_study = 'https://www.donostia.eus/kirola/betetzemaila/Piscinas_es#'
    else:
        link_to_study = 'https://www.donostia.eus/kirola/betetzemaila/Gimnasios_es#'

    # create a browser bot
    opts = webdriver.FirefoxOptions()
    opts.headless = True
    browser = webdriver.Firefox(executable_path="///home/ra/driver/geckodriver", options=opts)

    event = False
    counter = 0
    while not event:
        counter = counter + 1
        browser.get(link_to_study)
        # mine data
        raw_names_gim = browser.find_elements_by_class_name("zone-name")
        raw_percentajes = browser.find_elements_by_class_name('zone-ocupation')
        if 0 < len(raw_names_gim) == len(raw_percentajes) and len(raw_percentajes) > 0:
            nams_gim = list(map(lambda a: a.text, raw_names_gim))
            percentajes = list(map(lambda a: a.text, raw_percentajes))
            result = []
            for i in range(len(nams_gim)):
                result.append([nams_gim[i], percentajes[i]])
            browser.quit()
            return result

        if counter >= 350:
            result = ['Not availabel data']
            browser.quit()
            return result
        time.sleep(5)


def get_time(time_type):
    """
    get some time info
    :param time_type: 'hour', 'day'
    :return:
    """
    date_now = datetime.now()
    if time_type == 'hour':
        date_ = str(date_now.hour) + '_' + str(date_now.minute)
        return date_
    elif time_type == 'day':
        return date_now.day
    else:
        raise 'NOT CORRECT TIME_TYPE TYPED'


def save_txtlist(path, list_):
    """
        save the list to a txt
    :param path:
    :param list_:
    :return:
    """
    file = open(path, 'w')
    for i in range(len(list_)):
        for k in range(len(list_[i])):
            file.write(list_[i][k])
            file.write(' ')
        file.write('\n')
    file.close()
    return


def time_to_sleep(open_gim=True):
    if open_gim:
        in_time_ = False
        while not in_time_:
            minute = datetime.now().minute
            if minute == 30 or minute == 00:
                return
            else:
                time.sleep(50)
        return
    else:
        while not open_gim:
            current_hora = datetime.now().hour
            current_minuto = datetime.now().minute
            if current_hora == hora_inicio and current_minuto == minuto_inicio:
                return
            else:
                pass


if __name__ == '__main__':
    # configurations #
    piscina = False

    hora_inicio = 8
    minuto_inicio = 0
    hora_final = 22
    minuto_final = 30

    current_day = 17
    dia_final = 30

    path_tosave = '///home/ra/PycharmProjects/kirol_analysis/data/'
    final_dataframe_name = 'kirol_data'

    mine_data = True
    parse = False

    # # # # # # # # #
    if mine_data:
        print('# # # # START # # # #')
        while current_day < dia_final:
            current_hour = get_time('hour')
            in_time_hour = int(current_hour.split('_')[0])
            in_time_minute = int(current_hour.split('_')[1])
            # check if gim was open
            if hora_inicio < in_time_hour < hora_final or \
                    (in_time_hour == hora_final and in_time_minute <= minuto_final) or \
                    (in_time_hour == hora_inicio and in_time_minute >= minuto_inicio):

                # if minuto_inicio <= in_time_minute < minuto_final:
                info_kirol = search_selenium_kirol(pisci=piscina)
                # add time to info
                month = datetime.now().month
                info_final = list(map(lambda a: a + ['month: ' + str(month),
                                                     'day: ' + str(current_day),
                                                     'hour: ' + current_hour], info_kirol))
                if piscina:
                    last_name = 'piscina'
                else:
                    last_name = 'gimnasio'
                path_name = path_tosave + last_name + '/' + str(current_day) + '_' + current_hour + '.txt'
                save_txtlist(path_name, info_final)
                # time control
                time.sleep(60)
                # time_to_sleep(open_gim=True)
            time_to_sleep(open_gim=True)
            current_day = get_time('day')
    print('# # # # END # # # #')
    if parse:
        # read and save the data
        parse_data(path_tosave, final_dataframe_name)
